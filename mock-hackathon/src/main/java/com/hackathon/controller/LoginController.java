package com.hackathon.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hackathon.Service.LoginService;

@RestController
@RequestMapping("/login")
@CrossOrigin("*")
public class LoginController{
	@Autowired
	private Environment env;
	
	@Autowired
	LoginService loginService;
	
	@RequestMapping(value = "/getToken", method = RequestMethod.POST)
	public ResponseEntity<Map<String,String>> getToken(@RequestBody Map<String,String> loginInfoMap){
		Map<String,String> result = new HashMap<>();
		String consumerKey = env.getProperty(loginInfoMap.get("username")+".key");
		result.put("token", loginService.getAuthToken(consumerKey,loginInfoMap));
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	
	
}
