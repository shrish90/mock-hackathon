package com.hackathon.controller;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
@CrossOrigin("*")
public class TestController {

@RequestMapping("/testIt")
public ResponseEntity<Map<String,String>> testController(){
	Map<String,String> result = new HashMap<>();
	result.put("root", "working fine");
	return new ResponseEntity<>(result, HttpStatus.OK);
}
}
