package com.hackathon.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hackathon.Service.AccountService;
import com.hackathon.model.AccountDetails;

@RestController
@RequestMapping("/account")
@CrossOrigin("*")
public class AccountDetailsController {
	@Autowired
	AccountService accountService;
	@RequestMapping(value = "/getDetails", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Map<String,AccountDetails>> getAccountDetails(@RequestBody Map<String,String> tokenMap){
		Map<String,AccountDetails> result = new HashMap<>();
		result.put("root", accountService.getAllAccounts(tokenMap.get("token")));
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
}
