package com.hackathon.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.hackathon.*"})
public class MockHackathonApplication {

	public static void main(String[] args) {
		SpringApplication.run(MockHackathonApplication.class, args);
	}

}
