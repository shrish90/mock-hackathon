package com.hackathon.model;

import java.util.List;

public class Account {
	
	private String id;
	private String label;
	private String bank_id;
	private List<AccountRoutings> account_routings;
	private Balance balance;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getBank_id() {
		return bank_id;
	}
	public void setBank_id(String bank_id) {
		this.bank_id = bank_id;
	}
	public List<AccountRoutings> getAccount_routings() {
		return account_routings;
	}
	public void setAccount_routings(List<AccountRoutings> account_routings) {
		this.account_routings = account_routings;
	}
	public Balance getBalance() {
		return balance;
	}
	public void setBalance(Balance balance) {
		this.balance = balance;
	}

	
	
}
