package com.hackathon.model;

import java.util.List;

public class AccountDetails {
	
private List<Account> accounts;
private Balance overall_balance;
private String overall_balance_date;

public List<Account> getAccounts() {
	return accounts;
}
public void setAccounts(List<Account> accounts) {
	this.accounts = accounts;
}
public Balance getOverall_balance() {
	return overall_balance;
}
public void setOverall_balance(Balance overall_balance) {
	this.overall_balance = overall_balance;
}
public String getOverall_balance_date() {
	return overall_balance_date;
}
public void setOverall_balance_date(String overall_balance_date) {
	this.overall_balance_date = overall_balance_date;
}


}
