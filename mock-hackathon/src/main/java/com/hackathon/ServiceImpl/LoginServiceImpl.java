package com.hackathon.ServiceImpl;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hackathon.APIUtility.APICall;
import com.hackathon.Service.LoginService;

@Service
public class LoginServiceImpl implements LoginService{
	@Autowired
	APICall apiUtility;
	@Override
	public String getAuthToken(String consumerKey, Map<String,String> loginInfo) {
		String apiUrl = "https://apisandbox.openbankproject.com/my/logins/direct";
		String authString= "DirectLogin username="+loginInfo.get("username")+",password="+loginInfo.get("password")+",consumer_key="+consumerKey;
		String token = apiUtility.getResponseFromAPI(apiUrl, authString, "POST").trim();
		String value = token.substring(1, token.length()-1);
    	String[] keyValuePairs = value.split(":");
    	String trimmedToken = keyValuePairs[1].substring(1, keyValuePairs[1].length()-1);
        return trimmedToken;
		
	}

}
