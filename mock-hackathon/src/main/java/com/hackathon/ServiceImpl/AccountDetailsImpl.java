package com.hackathon.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.hackathon.APIUtility.APICall;
import com.hackathon.Service.AccountService;
import com.hackathon.model.AccountDetails;

@Service
public class AccountDetailsImpl implements AccountService {
	
	@Autowired
	APICall apiUtility;
	
	@Override
	public AccountDetails getAllAccounts(String token) {
		String url = "https://apisandbox.openbankproject.com/obp/v4.0.0/banks/test-bank/balances";
		String authHeader = "DirectLogin token="+token;
		String response = apiUtility.getResponseFromAPI(url, authHeader, "GET");
		System.out.println(response);
		AccountDetails accountList = new Gson().fromJson(response, AccountDetails.class);
		return accountList;
	}

}
