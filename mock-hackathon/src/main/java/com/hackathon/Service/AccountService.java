package com.hackathon.Service;

import com.hackathon.model.AccountDetails;

public interface AccountService {
 public AccountDetails getAllAccounts(String token);
}
