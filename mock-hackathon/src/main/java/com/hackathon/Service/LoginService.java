package com.hackathon.Service;

import java.util.Map;

public interface LoginService {
public String getAuthToken(String consumerKey, Map<String,String> loginInfo);
}
